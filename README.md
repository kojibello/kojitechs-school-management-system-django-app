![Lines of code](https://img.shields.io/tokei/lines/github/sajib1066/school_management_system?style=plastic)
&nbsp;&nbsp;&nbsp;&nbsp;![GitHub All Releases](https://img.shields.io/github/downloads/sajib1066/school_management_system/total?style=plastic)
&nbsp;&nbsp;&nbsp;&nbsp;![dependencies](https://img.shields.io/badge/dynamic/json?url=https://jsonkeeper.com/b/KNO7&label=dependencies&query=dependencies&style=plastic&labelColor=grey&color=darkgreen)
&nbsp;&nbsp;&nbsp;&nbsp;![build](https://img.shields.io/badge/dynamic/json?url=https://jsonkeeper.com/b/KNO7&label=build&query=build&style=plastic&labelColor=grey&color=darkgreen)
[![](https://badges.pufler.dev/visits/sajib1066/school_management_system?label=visitors&color=blue)](https://badges.pufler.dev)
# Project Name: School Management System Software

## How To Setup On Linux
1. Clone This Project `git clone https://github.com/sajib1066/school_management_system.git`
2. Go To Project Directory `cd school_management_system`
3. Create a Virtual Environment `python -m venv venv`
4. Activate Virtual Environment `source venv/bin/activate`
5. Install Requirements Package `pip install -r requirements.txt`
6. Migrate Database `python manage.py migrate`
7. Create Super User `python manage.py createsuperuser`
8. Finally Run The Project `python manage.py runserver`

## Cloning this Repo?

Make sure you create `.env` and fill in the following variables:

```
DEBUG=1
REGION=texas
DJANGO_SUPERUSER_USERNAME=admin
DJANGO_SUPERUSER_PASSWORD=mydjangopw
DJANGO_SUERPUSER_EMAIL=hello@teamcfe.com
DJANGO_SECRET_KEY=fix_this_later
POSTGRES_READY=0
POSTGRES_DB=dockerdc
POSTGRES_PASSWORD=mysecretpassword
POSTGRES_USER=myuser
POSTGRES_HOST=localhost
POSTGRES_PORT=5434
REDIS_HOST=redis_db
REDIS_PORT=6388
```